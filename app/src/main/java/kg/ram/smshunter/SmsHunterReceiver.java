package kg.ram.smshunter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

public class SmsHunterReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent i = new Intent(SmsHunter.SmsHunterLocalReceiver.ACTION);
            Bundle extras = intent.getExtras();
            if (extras == null) {
                i.putExtra(SmsHunter.SmsHunterLocalReceiver.ERROR_INTENT_KEY, "Extras is NULL");
                broadcastManager.sendBroadcast(i);
                return;
            }
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
            if (status == null) {
                i.putExtra(SmsHunter.SmsHunterLocalReceiver.ERROR_INTENT_KEY, "Receive NULL status.");
                broadcastManager.sendBroadcast(i);
                return;
            }
            switch (status.getStatusCode()) {
                case CommonStatusCodes.SUCCESS: {
                    String msg = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                    i.putExtra(SmsHunter.SmsHunterLocalReceiver.MESSAGE_INTENT_KEY, msg);
                }
            }
            broadcastManager.sendBroadcast(i);
        }
    }
}
