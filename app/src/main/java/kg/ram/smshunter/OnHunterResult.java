package kg.ram.smshunter;

public interface OnHunterResult {
    void onSuccessResult(String msg);

    void onError(Exception ex);
}
