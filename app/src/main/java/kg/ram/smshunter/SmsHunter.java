package kg.ram.smshunter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;

public class SmsHunter {

    private String TAG = "SmsHunter";

    private final OnHunterResult mOnHunterResult;
    private Context mAppContext;
    private SmsHunterLocalReceiver mSmsHunterReceiver;
    private LocalBroadcastManager mBroadcastManager;

    public SmsHunter(Context context, @NonNull OnHunterResult result) {
        mAppContext = context.getApplicationContext();
        mOnHunterResult = result;
    }

    /**
     * Checks GoogleApi service availability.
     *
     * @return true - available, false - unavailable.
     */
    public boolean isHuntingAvailable() {
        return GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(mAppContext)
                == ConnectionResult.SUCCESS;
    }

    /**
     * Runs service for monitoring sms receive.
     */
    public void startHunter() {
        SmsRetrieverClient client = SmsRetriever.getClient(mAppContext);
        Task<Void> startSmsRetrieverTask = client.startSmsRetriever();
        startSmsRetrieverTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mOnHunterResult.onError(e);
            }
        });

        startSmsRetrieverTask.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                registerLocalReceiver();
                Log.d(TAG, "Success connected to SMS Retrieved API");
            }
        });
    }

    /**
     * Stops service for monitoring sms receive.
     */
    public void stopHunter() {
        unregisterLocalReceiver();
    }

    private void registerLocalReceiver() {
        mSmsHunterReceiver = new SmsHunterLocalReceiver();
        mBroadcastManager = LocalBroadcastManager.getInstance(mAppContext);
        mBroadcastManager.registerReceiver(mSmsHunterReceiver,
                new IntentFilter(SmsHunterLocalReceiver.ACTION));
    }

    private void unregisterLocalReceiver() {
        if (mBroadcastManager != null) {
            try {
                mBroadcastManager.unregisterReceiver(mSmsHunterReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Return application hashcode for generate sms.
     *
     * @return hashcode for current application.
     */
    public String getSignatureCode() {
        ArrayList<String> signatures = new AppSignatureHelper(mAppContext).getAppSignatures();
        String sign = null;
        if (!signatures.isEmpty())
            sign = signatures.get(0);
        return sign;
    }

    public class SmsHunterLocalReceiver extends BroadcastReceiver {

        public static final String ACTION = "SmsHunter.SmsHunterLocalReceiver.ReceiveSms";
        public static final String ERROR_INTENT_KEY = "SmsHunter.Error";
        public static final String MESSAGE_INTENT_KEY = "SmsHunter.Message";

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (intent.hasExtra(ERROR_INTENT_KEY)) {
                    mOnHunterResult.onError(new Exception(intent.getStringExtra(ERROR_INTENT_KEY)));
                } else if (intent.hasExtra(MESSAGE_INTENT_KEY)) {
                    String message = intent.getStringExtra(MESSAGE_INTENT_KEY);
                    mOnHunterResult.onSuccessResult(message);
                }
            }
        }
    }
}
